﻿using System;
using System.Collections.Generic;

namespace Facebook.Post.Service.Domain
{
    public class Post
    {
        public string Content { get; set; }
        public IEnumerable<string> Comments { get; set; }
        public Visibility Visibility { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAtUtc { get; set; } = DateTime.UtcNow;
        public bool? IsDeleted { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedAtUtc { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAtUtc { get; set; }
    }

    public enum Visibility
    {
        Public,
        Private
    }
}
