﻿using System;
using System.Collections.Generic;

namespace Facebook.Post.Service.Service
{
    public interface IPostService
    {
        bool CreatePost(Domain.Post post);
        Responses.Post GetPostById(Guid id);
        IEnumerable<Responses.Post> GetAllPosts(string filter, int pageNumber, int recordsPerPage);
        bool DeletePost(Guid id);
        bool UpdatePost(Guid id, Domain.Post post);
    }
}
