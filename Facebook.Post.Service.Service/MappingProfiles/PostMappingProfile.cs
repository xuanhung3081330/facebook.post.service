﻿using AutoMapper;

namespace Facebook.Post.Service.Service.MappingProfiles
{
    public class PostMappingProfile : Profile
    {
        public PostMappingProfile()
        {
            CreateMap<Domain.Post, Facebook.Post.Service.Infrastructure.Entities.Post>();

            CreateMap<Facebook.Post.Service.Infrastructure.Entities.Post, Responses.Post>();
        }
    }
}
