﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Facebook.Post.Service.Service.Responses
{
    public class Post
    {
        public Guid Id { get; set; }
        public string Content { get; set; }
        public string CreatedBy { get; set; }
    }
}
