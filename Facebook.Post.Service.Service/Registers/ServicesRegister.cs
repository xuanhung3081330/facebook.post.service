﻿using Facebook.Post.Service.Service.MappingProfiles;
using Microsoft.Extensions.DependencyInjection;

namespace Facebook.Post.Service.Service.Registers
{
    public static class ServicesRegister
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddTransient<IPostService, PostService>();

            return services;
        }

        public static IServiceCollection AddCustomAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(PostMappingProfile));

            return services;
        }
    }
}
