﻿using AutoMapper;
using Facebook.Post.Service.Infrastructure.Entities;
using System;
using System.Collections.Generic;

namespace Facebook.Post.Service.Service
{
    public class PostService : IPostService
    {
        private readonly IMapper _mapper;
        private readonly IPostRepository _postRepository;

        public PostService(IMapper mapper, IPostRepository postRepository)
        {
            _mapper = mapper;
            _postRepository = postRepository;
        }

        public bool CreatePost(Domain.Post post)
        {
            var postEntity = _mapper.Map<Facebook.Post.Service.Infrastructure.Entities.Post>(post);
            return _postRepository.CreatePost(postEntity);
        }

        public bool DeletePost(Guid id)
        {
            return _postRepository.DeletePost(id);
        }

        public bool UpdatePost(Guid id, Domain.Post post)
        {
            var postEntity = _mapper.Map<Facebook.Post.Service.Infrastructure.Entities.Post>(post);
            return _postRepository.UpdatePost(id, postEntity);
        }

        public IEnumerable<Responses.Post> GetAllPosts(
            string filter, 
            int pageNumber, 
            int recordsPerPage)
        {
            var posts = _postRepository.GetAll(filter, pageNumber, recordsPerPage);
            return _mapper.Map<IEnumerable<Responses.Post>>(posts);
        }

        public Responses.Post GetPostById(Guid id)
        {
            var post = _postRepository.GetPostById(id);
            return _mapper.Map<Responses.Post>(post);
        }
    }
}
