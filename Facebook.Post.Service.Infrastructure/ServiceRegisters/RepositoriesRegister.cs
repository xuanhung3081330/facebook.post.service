﻿using Facebook.Post.Service.Infrastructure.Entities;
using Facebook.Post.Service.Infrastructure.Implementations;
using Microsoft.Extensions.DependencyInjection;

namespace Facebook.Post.Service.Infrastructure.ServiceRegisters
{
    public static class RepositoriesRegister
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddTransient<IPostRepository, PostRepository>();

            return services;
        }
    }
}
