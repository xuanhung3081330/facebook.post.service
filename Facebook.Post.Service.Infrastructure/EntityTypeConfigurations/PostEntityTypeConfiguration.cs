﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Facebook.Post.Service.Infrastructure.EntityTypeConfigurations
{
    public class PostEntityTypeConfiguration : IEntityTypeConfiguration<Entities.Post>
    {
        public void Configure(EntityTypeBuilder<Entities.Post> builder)
        {
            builder
                .HasKey(x => x.Id);

            builder
                .Property(x => x.Content)
                .IsRequired();

            builder
                .Property(x => x.Visibility)
                .IsRequired();

            builder
                .Property(x => x.CreatedBy)
                .IsRequired();
        }
    }
}
