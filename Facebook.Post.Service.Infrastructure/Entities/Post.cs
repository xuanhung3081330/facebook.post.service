﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Facebook.Post.Service.Infrastructure.Entities
{
    public class Post
    {
        public Guid Id { get; set; }
        public string Content { get; set; }
        [NotMapped]
        public IEnumerable<string> Comments { get; set; }
        public Visibility Visibility { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAtUtc { get; set; } = DateTime.UtcNow;
        public bool? IsDeleted { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedAtUtc { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAtUtc { get; set; }
    }

    public enum Visibility
    {
        Public,
        Private
    }
}
