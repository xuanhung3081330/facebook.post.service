﻿using System;
using System.Collections.Generic;

namespace Facebook.Post.Service.Infrastructure.Entities
{
    public interface IPostRepository
    {
        bool CreatePost(Post post);
        bool DeletePost(Guid id);
        bool UpdatePost(Guid id, Post post);
        IEnumerable<Post> GetAll(string filter, int pageNumber, int recordsPerPage);
        Post GetPostById(Guid id);
    }
}
