﻿using Facebook.Post.Service.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Facebook.Post.Service.Infrastructure.Implementations
{
    public class PostRepository : IPostRepository
    {
        private readonly PostContext _context;

        public PostRepository(PostContext context)
        {
            _context = context;
        }

        public bool CreatePost(Entities.Post post)
        {
            _context.Posts.Add(post);
            _context.SaveChanges();
            return true;
        }

        public bool DeletePost(Guid id)
        {
            var post = _context.Posts.FirstOrDefault(x => x.Id == id);
            if (post == null)
            {
                return false;
            }

            post.IsDeleted = true;
            post.DeletedAtUtc = DateTime.UtcNow;
            _context.SaveChanges();
            return true;
        }

        public IEnumerable<Entities.Post> GetAll(string filter, int pageNumber, int recordsPerPage)
        {
            if (string.IsNullOrEmpty(filter))
            {
                return _context.Posts.ToList();
            }

            return _context.Posts.Where(x => x.CreatedBy == filter).ToList();
        }

        public Entities.Post GetPostById(Guid id)
        {
            Func<Entities.Post, bool> func = post => post.Id == id;
            return _context.Posts.FirstOrDefault(func);
        }

        public bool UpdatePost(Guid id, Entities.Post post)
        {
            var dbPost = _context.Posts.FirstOrDefault(x => x.Id == id);
            if (dbPost == null)
            {
                return false;
            }

            dbPost.Content = post.Content;
            dbPost.Visibility = post.Visibility;
            dbPost.UpdatedBy = post.UpdatedBy;
            dbPost.UpdatedAtUtc = DateTime.UtcNow;
            _context.SaveChanges();

            return true;
        }
    }
}
