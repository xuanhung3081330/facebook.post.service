﻿using Facebook.Post.Service.Infrastructure.EntityTypeConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Facebook.Post.Service.Infrastructure
{
    public class PostContext : DbContext
    {
        public PostContext(DbContextOptions<PostContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PostEntityTypeConfiguration());
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer("Server=localhost;Database=PostDB;Integrated Security=true");
        //}

        public DbSet<Entities.Post> Posts { get; set; }
    }
}
