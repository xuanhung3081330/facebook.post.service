using Facebook.Post.Service.API.Registers;
using Facebook.Post.Service.Infrastructure;
using Facebook.Post.Service.Infrastructure.ServiceRegisters;
using Facebook.Post.Service.Service.Registers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Facebook.Post.Service.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            // DbContext
            services.AddDbContext<PostContext>(options => options.UseSqlServer(Configuration.GetSection("ConnectionStrings:SQLServerConnectionString").Value));

            // API
            services.AddCustomAutoMapperApi();

            // Infrastructure
            services.AddRepositories();

            // Service
            services.AddServices();
            services.AddCustomAutoMapper();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
