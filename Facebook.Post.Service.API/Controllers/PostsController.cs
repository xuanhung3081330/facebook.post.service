﻿using AutoMapper;
using Facebook.Post.Service.API.Requests;
using Facebook.Post.Service.Service;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;

namespace Facebook.Post.Service.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IPostService _postService;

        public PostsController(IPostService postService, IMapper mapper)
        {
            _postService = postService;
            _mapper = mapper;
        }

        [HttpPost]
        public IActionResult CreatePost([FromBody] CreatePostRequest request)
        {
            var postDomain = _mapper.Map<Domain.Post>(request);
            _postService.CreatePost(postDomain);

            return Ok(HttpStatusCode.Created);
        }

        [HttpGet]
        public IActionResult GetAllPosts(
            [FromQuery]string filter,
            [FromQuery]int pageNumber = 1,
            [FromQuery]int recordsPerPage = 5)
        {
            var results = _postService.GetAllPosts(filter, pageNumber, recordsPerPage);
            return Ok(results);
        }

        [HttpGet("{id}")]
        public IActionResult GetPostById(Guid id)
        {
            var result = _postService.GetPostById(id);
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public IActionResult DeletePostById(Guid id)
        {
            var result = _postService.DeletePost(id);
            if (result == false)
            {
                return NotFound($"Post not found with the id = {id}");
            }

            return Ok();
        }

        [HttpPut("{id}")]
        public IActionResult UpdatePostById(Guid id, [FromBody] Domain.Post post)
        {
            var result = _postService.UpdatePost(id, post);
            if (result == false)
            {
                return NotFound($"Post not found with the id = {id}");
            }
            return Ok();
        }
    }
}
