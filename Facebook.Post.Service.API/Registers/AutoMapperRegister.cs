﻿using Facebook.Post.Service.API.MappingProfiles;
using Microsoft.Extensions.DependencyInjection;

namespace Facebook.Post.Service.API.Registers
{
    public static class AutoMapperRegister
    {
        public static IServiceCollection AddCustomAutoMapperApi(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(PostMappingProfile));
            return services;
        }
    }
}
