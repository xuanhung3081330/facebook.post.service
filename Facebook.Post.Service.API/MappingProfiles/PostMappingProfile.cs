﻿using AutoMapper;
using Facebook.Post.Service.API.Requests;

namespace Facebook.Post.Service.API.MappingProfiles
{
    public class PostMappingProfile : Profile
    {
        public PostMappingProfile()
        {
            CreateMap<CreatePostRequest, Domain.Post>();
        }
    }
}
