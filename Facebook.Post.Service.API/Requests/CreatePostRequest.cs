﻿using System.ComponentModel.DataAnnotations;

namespace Facebook.Post.Service.API.Requests
{
    public class CreatePostRequest
    {
        [Required(ErrorMessage = "A post cannot be created without its Content")]
        public string Content { get; set; }
        [Required(ErrorMessage = "A post cannot be created without its specific visibility")]
        public Visibility Visibility { get; set; }
        [Required(ErrorMessage = "A post should have an owner")]
        public string CreatedBy { get; set; }
    }

    public enum Visibility
    {
        Public,
        Private
    }
}
